
ORIGIN=`pwd`
DIR=$(cd $(dirname "$0"); pwd)

# Needed for ahni
sudo apt-get install ant default-jdk
mkdir $DIR/ahni/antbin
cd $DIR/ahni
ant runjar
mv ../ahni.jar .
cd $ORIGIN

# Needed for MultiNEAT
sudo apt-get install libboost-all-dev python-dev python-all-dev libopencv-dev python-opencv python-pip
sudo apt-get install python-matplotlib python-scipy python-pygame
sudo pip install pygame pymunk progressbar
cd $DIR/MultiNEAT
sudo python setup.py install
sudo rm -rf $DIR/MultiNEAT/build
cd $ORIGIN

# Go back to the calling dir after everything is done
cd $ORIGIN
