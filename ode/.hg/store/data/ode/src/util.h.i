        z  �      y��������H����m��#��@��'�~�            x���Qo�0���)N�T4-<���J$(	b}B�O��l�}�]��)ڴUb~ bs���w����r�.���%+�
�6���؁��V�or�Az���}� ,�A!`�BTp��M�G� ��t������G��*���%&������&�	�97 x����~f���i�C�JX3I�Sn��Ii�&�;�?�
��OK���#Xԅ���c?|�GU �n��!<M0A�U�J�L��L_Ä�Qf`[�SH��kGuM��׌`�H�\�> ���;Ԇ���vk�u�+��c��(�D��ֈ6�P�`���T[|�'V#��
�!#*�kQ��О4�Y$y���2.Hv0�����S�}�������hxklEL�*����_�����6���3��w��7uz�5W�S��r��MJ�Y):M���z����>��Co?��(:����
^;ƴf�Vug4Q_�p0�X�1��s�t� ��Q�Y̽0���|�gTb���5o�F�3z�;d�.�A��.��B\��K�;ǹ��-�fC����j�r�i��叛��c�J���7�rg�x
i if%c2I�WZ5����җ��"��}�Ձ4DRcqk�Ol��r��.;����    z     �  �      �    ����6i#0����Q�jSA/c���\            x�mN�
�0�䐧��-}77�D����]��$!4��|C1)Q<8������<ĺ�y5#��'�3�@�a�UW����Rn��%�VzQ!G�%��|�D�q3BuK'*�	�F��f�0j��%��p0����)�L��R�wJ=rݣ3=1�yn5��a��}!����XV           �      K   ����ς� �Y�V�7�A�ф�)�            x�u�Ok�@���)|�W���ЛI�bp�Cr*�0�F���#vW�U��{���mp�Ү����F����?��[�kU*�P��5|��q!B�1�Λ0�@�!Dߖ�� :�P��)ȃ���"�)md�
�?넽�.�=����Xh���4�����Jg�ɿV��W�*93Tk@E��i�������k=�~��p���+^LE<4DU6��`��+����@�y\�}O";f�l����;>�r
r�/A��CA����!P��n����C�-쐵�����2�4,%uC��.�����!�CKL�i$��]ߢ���������TTc��f\&cU[^�����f��z^�W����.�tn�Qɘl��$Ix�ڨ����g/I���%�����H�ڊ�/�$�st���W�Wߗ�>���}�i��NϜ���b��p���!m���w4sb�]<��m�}F���Q9;�H	����1��J6i�L-�뇛�l��~$���-�I��m����c��|��=�r%r��H    ?     b        �   ������h�+:xq` '�|R
W              �  �   V
/* Makes sure the matrix is a proper rotation */
void dOrthogonalizeR(dMatrix3 m);


    �              �   �����
��Nw�:���T���H                �       �      �   ����!0��(�㕇W$���B��2              �  �        �    �  \         ����/���!@�V#y�8wO���x            x��W�n�Ffo�|�0E a��z���K*"ǐ'텥ĕM��%v)[n�>N�8=�-���!�_�A�����|3�����q��yv�8ο�C���ЛL��S�r�/��xd�!���+�o��3���{ׅ>�A��~�~�vmr{-�W�9I�=�B�u�hZx;�,��_l~�3I��*�-�E#���s�޷��s��t}�J*� ��: �u�o��v��kG�;z��?�d&����{.��J�5�rF.gn�����S���	��S�?>���0ـ��;j���"b+�Fe$)�XF�*h��e���	FBE�C2�J��7���C�T����a�ɢj�eC���	J�W�@�[b�C-3�$��J�%� h�����_���� � *�f�{gJ_6w:�}�zt�$�C��_��h*;��M�SI�=��׷�3{�&�RjE�`�q<�-���!l#��E,J���*'�bA�KMvݔ4��P��8�#x��IjՁ�T�u$#�$r*�q4
2dn��T�ތ��A+y���.C��t�n2����?�{�c�������m��$U|CZ}���(�h+�c��1�Qpť�V1��;�n6T,TqY� Ͷx��6� ���>-�k�����V q���8�E��S�,?�b��̂�tWD��np��
q����}
������]=�2��Z��8�Y,��@���8
�k�dV��]�W�����}\��~�^����XY౰�?9�ҟ��
��퐊��I�d݆���n�y��2����R� �7cc��l't-Ǫ9�-V�0�ꖔ�A��F�C3��Cy���3����(+��4'K`���|�={4�������/I�zg�nˬ�)��h�Ƙ���,H(x1Mc��׸��ܧ;"x��]�-�uG*�n�(m�����u��f��VP�e ����e!`��v�e�R$�vu�ێG��:r+��=+�K���Qx�����l���CP]0�5X�Q��+",�K	t}��ȕG9��x��GX݊����^���N���V\&"�����q��s����f��^s�lf����<}��5��3\̵��2��S: ��j˧T��F�������R{�+���CPl%�T�{�XY�e-�RM 9ҏ�
���L�otU�Q�R�j���-D��F�oӂ��j�o��̻�^���x����G��� p�46�ج��ui0�
0���CP�qe.ىԫ}�
eF��h�oQ���4�;��c6��A��W�k��[KV�\�m�.��gw�3G2AEe'KOG�lH��'Eڈ����r�`���,يD��i2���N�\Hz��V��H#���KE�6t��;�0PO+g!j���M���[K��/�"��`OX5�/�u����%�    0    1  (�         ������PRȡy�1����I�d�            x��WKn�FmdI�
�M~^�7G������(bŁF���,ʌ�&�nJr��r�,r�� ��"GH5��o�ȆpfX���_]bl�5c�1ƾ2/|	���4�����G!1I�7���0�q ��4�|�z�>���;OI<�U��ǩ�<���eȗB��HS��wY��6�0G�r��2!�w������":�n��B�3��ơ��4
���&�kGْ,�V����AJ��ٰJ�P?f������\�fEf�P`~�<L-�����?	ba�NK���#oi��e�O��*T@�aģ�HT6������>B_К�w��
wGH��C�"܇��}���'��a?�Ĝ���-��gzE,[f�C������z��1=�ۋ2���>���Q
[��k
�z1���~EzW?3�u	������M���%��4Mc�V�T��Ķ��Krp���5P�`�BF	atV�R��l\:ΣG�Q˃?B?W�I?HOA_:~�	A6.��K}BxP�׎~U�yw�cR�ӂK��ɔY��q�����s�ͣv4�$����T�$rA++�Z� r�D a�d7"NA���`�\�e�L5�%Oc��R&q�Q��s��aWݏA�c�6�-��o�����ب����w�!��
��v����%Sc�^�W��R7Vo椝Y���$����#�G3GՒ���l-���6�U��S*/��eA�ʮvV�*��QNx���>R���:7��צ��ʻ�eds�[ۥK�T�(r|?5�ݐ����N�?�]۫ێA&-���[�ƥ� ��Qf9^�_n�����Ԅrة��f�̓��~[U5f2����l�ҟ�kJ���;(���'���-�u�R�j��_��,_%�j�^/=�fC�0z�0�У6�0� ���*���m�odW�����;�nx�Z;L=q���e0r�,3ض�����rC˴�$���o��8W��o2|�D<�86��3���ڬ��A��+B�8*�-��E�#�՝�n���LM���ÃM�� ��jE�Yd��'VoTf2�N� �lF    a     D  (�         ����10�� ��������E\�            x�c`�ff`�fa``�T�L�KIMS��r��u��R�2�R�

řU��%�����\ʩy)�i\     �    	  *\      .   ����Ҳ$5�n�����?����v�            x�}��J�@�כ��Iă0�ڥ�=Ԟ���xʚ��hv��nJj�|/��mӈ�����|�?+����K$ k�4���)
����F!'�|�`�[0�մ�)�5�h}f�Q$��G'q��s'��DTC�RF�㔯B쌬��O��J��/�폚#"?p=����<�B\�u��} �uʔ���A�6�C�͊Xm~Y��S���=��QW��҃A��::�|�.�oʪdX5%��dw���d�ט؂@rg�`��a}ȶ��    �     V  *�      /   	������cW3�qA���e\`��Q#            x�c`�?� �>

iE��y)
�%E��%
)N�ũ�
��
!�
�e��)
���
�Ey�y�
E��E) VAQfYbI�BJ*Dg~ ���          +B      X   
���������4���ɥ��e��            x�}�AK1�ǋ��x+8P�e=x��⡂PZA���Y��ɚĲ�����-�I�*�$�_��'Xp,�Q�p>y��7/ȹS_$|�/��<8=	�8#.��8��4�T���+��v\i�ʵ����Ծ�h�!�X$?u%�����la�J���O��[�?��N�����l3#o�bCD�bb�����w\���&��,tcu�p>
�Ư:
�H_�t����htXr�?�ʩ559wg��>��C9'0�ma�#��    
     g  +?      d   ����T��. 5����yJ�
{��            x�-˱	�0F᫝��/U�p �,Er'B�St0��0݃�GTOD�KDF�(,;�`M�Y%FI��Wm���xN�H�c�*��3��0 �
����Fq��� ��%�    q     U  *�      }   
����<�`,$�[ZV�lݷ�t��            x�c``a``+a  �d8�T����)��g�(hh����ŧ�ŗh*h�T����'�;�畤V�(h%C:
\ ���    �      *�      �   
�����
��A�Q��F+\�)٢=            x���=O�0���!+ *��Q&V$�`B��h�����(��F~+?�?���"U�K'����}�8>8988D�X%q�o讦�,m����%N=�`T�̍,��p�k�ԃ!��0^Řv��v�핕����Jեom(�prӋ���׊�!Ŀ2�?A��(�/��7 p���jq�v���;g-�N5ޡә���;�`����֥��1l#�����eY4��؃^��;¬|�?tLU��l�y��q���H�r/I�&M��Ι���b�76����1>��    �    �  0j      �   �����o��+`;�`d�S��.(            x��W���D�k�J�`�rɆ0�$.s}|Fv�3wc;�a�Qt��(#���䜹9Z
**J�
Zz
f��J�>��-ݙ���{���~�F��/]�!���4��ه1�X�4=!��u�k>	�nF��uS���1%/�}�Ey�ag��`�C��i�p�i�baYC���)��I~�}��O�%���߷G��c����MF��~��Éc��'�����<|l�i���z3r�0��k;�~���8�rFq�$V�^���wwK�M��V�������6��0Z+*`_�]����4a�fnx-��戸����dm
q�����s2����{Xhy1�j���lq�RwF6A>��=<c7�,e��e1Pr�3
���(�A�GF��a>�3R�چR@]��r��j�ZRT��zPh��ҏو���Ɉ<�F"�Q�+aaY�q  �O��4�+% |!�K�l�7U ��XG��hZ����4#gSv�<uڹV5E��.r�y+��q�A:�X��I O�0�uC��ڢD���4O�U�*uD�A��/i�=|^;o����H_\nr��$!�
�J�4y��Ꜭ�U}Ae�j-���H�HP2E)zk�����_y�8'�Sr5�լ�O�k��)h��m��� �.���]�|�k"�o
���/>ꭳ�Q�Uq�W��J饆 GqEM�i�^l���V�=b^��
�B��7� �N��E7��T���$�����g|���\�P�H4=�2Us�Q���7-%��R�+�S�-��7�K���+�G���/@� ���є�Ҏ@�8Oj�b>-eL�0,��E�	�/���2�vu�_n@�c�Fh{��g;�{�u����Cg<�&�3���:����$t��Sl ��ރ�;��T>�s����*u0������ �f���Q�^7�T��3�Nf����G�v�{�v^k�|@J+�|���5N��i»Es�z��V́�n�}#V� ��
� �W�V��vצ������\�U��:����my��,xB�$f.[4	��֯�~�f�\��w��w��z#K!�8����;�fy�4�=XR�Y`~DB_P_��v� �4���ê��z�c��R^�h�)��V������'�Y�nV��X���M�G	�g��]�W?��$�Tf:S��9}��o@������<�<|||,,~'�-�Ll�����ΫL�F5��j�����"�k.���#B�o�ۿ     �    r  6�      �   ����~��(���,�ަ�!J��!�            x��VM��D�
NA��-�>��i�*�i�EdSg��MVq�R�&�dk��flo���O����w�@�7z	�'ޙ��8q�ێ�����c�j��}�����s�`�.Ǝ��C�ѰN</�I�δ����3����4���.B[�D�aX��	gط¶ޣG.ï����^oo/t攽V��w|5,�ճם�s����Y܆-�����q5W����]�R����-�o}�-D᭒(��h	�߀�������T�\~���y�L����N���z�&�<"ǁ����?[�_&y.8g����ZTU�~]*n��=��R�m�ԇ���kE�_��|P�TPRf�p�ā��A�bܐƃI]��vF�j�rͬО�[i��i�e�̕��cpev/��s��F�����>����E]�v���fY�o��*���ji������.1�4a6��~eX����{E�ͬ�K���@��Qv���`���v<JXJ�C:� �3� 8�����x�o�^��K�a2�\{�0�,J85c���sfƟR;� �=�T�v`��#nl���5#�s"��eG�`=y�=-��!nJ�`����ie�J+��S�b�E�^ }q��H)I��w)�S�h�E��f���N�uD[�a`Ř�|��C�y�r� M1t�m)B&�#ҵ�K]N�2�s���}O���B���~�B��*[/M����$�Z��Ro�1����IAZt�_h�J�����R�.�\F/���j�����r�>�9�E*֥*�!����(b��
ontq�~�FY=�v�������0>��I�K<�*5`K-x�y�0�e�1e��R8���b6'�����v,s��C�=��mk�7j�fɹx��ܷ��4���#]q-�Z��˳�H}�k�R������N���O��s��qqZ������7��x�� �ƫK��4�bI>)"�� 	a��K��#gY�ָ(Q>��9�'�cu��>Џ�9����Y*�׆��b1&)��&�v�?P�b�B2hh���r0���9�����g(�"��/I�M[Ԗ;��ᡝ��ӌg�̚�;�-�U���{�*k�JL/�5�&�    I    �  9�      �   ����i��MV(zi��R��N�"�            x��ZK�G.���cǏ$6.?dffw�H��z�f׳f`_���zgjv��Ǹ�g��D@�!'� q�%"!a�)A�r�1 N� �(�@� BH|��w��l#�nOU����꯿�������cB�I�O�23S��V���F�����
�8RGʳ���sx*�[	y� ���%Co����2>�B�h��>�mtY�u読�i�P��K�s�ٱ�^���Yӣ��)Nl͢tWݺ#�C��k�ǜQ���9�<�71䚐�9���R�a�!���0	ٶ�z]{����E�n1םc��l�i��̜B�2�c�y��c�.�FiBF�̠�(Թ#��O�u�e�p����R��!�hkp�a�#4V��KͦbF'}��A�N����0�)�������:���Ҳ�prFR��w�Gh�䬄m�"d�w1�J��k�e�*�Z��1l�Q]3Z˳]=�۞�iOW#s���=3��I�$Y���/��K��IO?�(0P��;&��]�&'�B�wD�D6�c��I�	:>N/�Lk.�:����mѶ�\j� �<,^��&����n~�Bv� �M2����K�mЪ[Ƃ�mXn�`~6��z�s�<$��0��X�^�ZiΕ/�1:uaf�Rk6��Ze�ܬ\n����d��Ъ����Ynρ���A����*s��Z`����~�݈�J�9�&�be�HNȓ;t����z
�^6|�jx�e[j$f� ��y�2�)ޣ�U��^��E��ۅ"mٖ�e��ͮHSlY��I:av�;T�vV��f26/`�?�\��喱�̳uO؄N*�w����b���S3�$M����_G{>ԅ���3O�/Ἶ&T�-F-1�ꕨ!���Y�փ�T�k�����o����ϡ]�k{�xd y�flS��	�ݗ�^��(���c�ݯ��υvZd�\5fj��Fe����� �w$������f,�H�Q%n�-�33�J���W
���Ċ!�v�\�Wj�~+-�H#��:�sh�=$d����1�k�gx�hi&���@h��YA�MVvm�7Y��Y^N�+�ȋq�BZZ۝��bd��{��}�Fԇ�X�K�?RΘ�V���2[��&p�?��ݧ�rX���O��v4%��]|�������CB�@��ޣv�T�'�J5�)�}��$9D0�Ɵۛ�tح�pK�߁��@c.C/Y(9���I�z�ӌU�J&�
�ΓUSƇM~Gȓ�b����n�^�;�?}�;��ȣ,��.pL�	�<~r��\�Z�	�N|���'�'�SPىO��ܾ�s����Uy�ح���r�<!�(\<���<��S��a&���"[N| �2�T�M����a�Qq@�ܲ�Y�c���9���O�-bp"K�6���^A���Q�x�e�9�2m0��uI�a'e��	��]OA\>�r�8���|D�Y�z�c�.J����t��<~`�3y�/W��4��%M���x�Uu�j���n-����%[k3eDx���~�pa�C����b��5�����L-������������D	��) ���0������i�O��Kl��4��z(m�4�Z��"�,�Xc�aZ��T�����~���V� ���]���+똀P��%�B���;�ܜts��܌I�d3��l;Iକ��K�^%��^��^T��ጓR$F�iM��ٺ��Ie�#�02ٖ�C��bCC}�C�E����p6*랃�C������'2c�1F��i���4��f$z9G���G�F��Y���ŹKv{��]3��X,�˝,c�T��>!Gv��7��/�����7Yl�/N�5���b���I����h��j�ܕ&j�jy�z�2
�#��j��2��wV���[���֊��#�ͦ�ގ�:��{-�����uB��k����iQ$�^�5@K�6:����<JI����{5��b�"z�w�^r�� ~�S�[8��do�0��orl��Gv1#�f�U��tA��`A��-O��$'��.��)���N����o2�����������	�(;U�Y��u�Y?6kJ`c�"6&��cR��]~x���&��Jmi�l�e����83GF�ü�$�H��y9����&�u>9.�����p��/�r��	���	��6�|����Uڃ�2�����I�h�Hİ�&�\�J6�MSM�QӺ@^�!U�n�j�x�g�g�lL2G�]��_�yB>ȋ�9Æc�����\�]�N�R*��(/q����o �K��b�4ޏ&򪊖�EǗ�X4���=t�4mh�+/��-O8�ݖ�4-T�Y�h틜V��¡4U21e��Z�]�����=A������Q~�<�{�:�"���h,�:j����d�<��D��f-O�wN�̞.���m)�c�`�v���}s��Z?x�,F����Ǆ�}I>�t�Ym�"��o�l������4]��PYs��	���=�f��v�v�b�{�Á���:̆�)Y�`�c9JOdL�i�5!a�Эc"���0�u�#��v�a��}|py/���D$�FF��!�񯠽�]N`>�2�}�����C�Kbl,6c��&܉%LeoE��B���~B���B����D�?|7~:oe�S�
*4�8NM��|1����A������w��V/��l�M5v6G)Cܪ�o��+�:�>4z�e]?�R��0l�ѱ�b/���[�`�c&������ �V���������Jӷ8֐F��0���`�k[)4$�2� �_Ow��ǖ�Q�s���/C>���_�Q�
�`� �7vv�(�����ifj�{�a�"6/@S��2m*��韷d�"N 
b�8!'�>s�hERR���y���rFn�>����1��LE휸��.�>taO���X�+�(}�_��!��d4;r��o������}\�I?������Z�����o~Y��Br��<�����yO�R��'g��W)L���ޠ�H���?�8��    )"       9�      �   ����(�4�����|�V�@s�6              $5  $I           dxPCM__MAX
    )A      ?h      �   ���������;Ɖ�!c~2,/{b�            x��U�n�0֩?�n�]�����ah0�h�	p��R�tE�l���l�~bY��& ?���i�F��7B�"�~a}$�	�����K�6�.Ğ�*�U{.�=�ւK��m����v)-��}H�F���������@������k���Zi�J�nݗN�AJ��M�.WpP٣qʈ�Ý�Z���O��-4-��g�e{��7���7��֩�J�y�.ϟ��،W��TyLylb�4&���ݶ6����K-{f��9j=
�v�x��B� ��^_�e#aI��P%)�2�gډ�J�^oh�'�����Q|)��s�����������-㻀s�x�|�-NtUH�.��̦ƀ@W�U�J��-;�e�=�$w���;P��OV�b�u���S?̒<,���T��D?�M�����L�^��;<��􉏮uiGrTV�^f��o���I�7�_�������p8�(��T^��g���m>���i��T�0��'����P�G�o��(ll_���j�^������ �U��&]�    +Z    �  @�      �   �����#�4�þ'K���m�":��*            x��S�J�0�����$-UA=��aA\����F�dm���W�CПЃGO�<���N�+�3o��^ � }�{��Jy�'RY�je,�'·�ɘ�y'�v��dW�[�p30fd��׎�	t8�A�=� _�f`�x,��B����z,�zZY1��ONt�q߸OB��`1X��K*&��d��������t�3�De���B1O��_�PK�� q`Ya����yH�EPd�_�ȷ� ���F[�(��j���$i������۪��;�sWE�F#������^h�Y芖.���pa�Q��`�%\���c�Ũ"�1��$E��n1�X�3+�#5��3K���ӯ��4Vo蓑��
��Į�Zg���ZxM�����ח%��F�,��f��i>^��K��
�+�`&�8�셌.���'��(	~k�a    -     �  A
         ����P�g�QB?\���aS!���r�            x����
�0D��WTh��wO��Ph14��%�$�`͖&H�K��-zt`���Y�����&^wԒ(�b)}C�Ԡ�N?���~�O����fCDfɑ&%guFiV�8M��<֜eU�9�xz�ح}��G��j�� �֩�Wow!	��I6��BY��%i�.A���H$~���Jy