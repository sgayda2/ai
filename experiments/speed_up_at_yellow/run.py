#!/usr/bin/python
import os
import sys
import time
import random as rnd
import commands as comm
import cv2
import numpy as np
import cPickle as pickle
import MultiNEAT as NEAT
import multiprocessing as mpc
import scipy as sp
from scipy.interpolate import interp1d

NUMBER_OF_RUNS = 1
NUMBER_OF_GENS = 400
NUMBER_OF_FRAMES = 1000
NUMBER_OF_INDIVIDUALS = 300
TIME_PER_FRAME = 0.01
MAX_FITNESS = 1000

SHOULD_SAVE_ALL = False

class FakeWriter:
    def write(self, someStr):
        pass

    def close(self):
        pass

class WolframAlphaCalculator:
    def __init__(self):
        pass

    def getDesiredSpeed(self, position):
        if (position < 0):
            return 0

        if (position > 0.82134):
            return self.getDesiredSpeed(0.82134)

        x = position * 10.0
        y = -1 * (x ** 5) / 280 + (x ** 4) / 14 - 11 * (x ** 3) / 24 + 13 * (x ** 2) / 14 + (97 * x) / 210
        return y / 10.0

class LineSegmentCalculator:
    def __init__(self, segments):
        self.segments = segments

    def getDesiredSpeed(self, position):
        if (position < 0):
            return 0

        for speedSegment in self.segments:
            if speedSegment.hasDesiredY(position):
                return speedSegment.getDesiredY(position)

        last = self.segments[-1]
        return last.getDesiredY(last.endX)

class LineSegment:
    def __init__(self, startX, startY, endX, endY):
        self.startX = startX
        self.startY = startY
        self.endX = endX
        self.slope = (endY - startY)/(endX - startX)

    def hasDesiredY(self, givenX):
        return givenX >= self.startX and givenX <= self.endX

    def getDesiredY(self, givenX):
        return (givenX - self.startX)*self.slope + self.startY


class WorldState:
    def __init__(self, middlePoint, endPoint, calculator):
        self.calculator = calculator
        self.middlePoint = middlePoint
        self.endPoint = endPoint
        self.reset()

    def stepInTime(self, delta):
        self.time += delta
        self.mySpeed = self.newSpeed
        self.position += delta * self.mySpeed
        self.desiredSpeed = self.calculator.getDesiredSpeed(self.position)

        # TODO(stoyan): Fix this calculation to not be based on adding
        self.desiredLocation += delta * self.desiredSpeed
        self.distanceToMiddle = self.middlePoint - self.position
        self.distanceToEnd = self.endPoint - self.position

    def reset(self):
        self.time = 0
        self.mySpeed = 0
        self.newSpeed = 0
        self.position = 0
        self.desiredSpeed = 0
        self.desiredLocation = 0
        self.distanceToMiddle = self.middlePoint
        self.distanceToEnd = self.endPoint

    def printStart(self, output):
        output.write('#Time, Position, Speed, Error, Speed Diff, Desired Speed, Desired Location\n')
        self.printStep(output)

    def printEnd(self, output):
        self.printStepPriv(output, True)

    def printStep(self, output):
        self.printStepPriv(output, False)

    def printStepPriv(self, output, isEnd):
        error = self.getError()
        if isEnd:
            error2 = error + self.getEndError()
            #print 'Error:', error, 'Error:', error2
            error = error2

        speedDiff = self.newSpeed - self.mySpeed

        output.write(`self.time`)
        output.write(', ')
        output.write(`self.position`)
        output.write(', ')
        output.write(`self.mySpeed`)
        output.write(', ')
        output.write(`error`)
        output.write(', ')
        output.write(`speedDiff`)
        output.write(', ')
        output.write(`self.desiredSpeed`)
        output.write(', ')
        output.write(`self.desiredLocation`)
        output.write('\n')

    def getEndError(self):
        diff = self.endPoint - self.position
        return (max(0, diff)**4) * NUMBER_OF_FRAMES

    def getError(self):
        return abs(self.newSpeed - self.desiredSpeed) + abs(self.desiredLocation - self.position)

def evaluateFrame(state, net):
    # TODO(stoyan): Prety sure this is constant and always 2
    depth = 2

    net.Flush()
    net.Input([state.position, state.mySpeed, state.distanceToMiddle, state.distanceToEnd, 1])
    [net.Activate() for _ in range(depth)]
    o = net.Output()
#    if state.time < 1:
#        state.newSpeed = state.mySpeed + .002
#    else:
#        state.newSpeed = state.mySpeed
    state.newSpeed = state.mySpeed + o[0] * .05
    return state.getError()

def getNewWorld():
    speedSet = []
    speedSet.append(LineSegment(-10, 0, 0, 0))
    speedSet.append(LineSegment(0, 0, .2, .2))
    speedSet.append(LineSegment(.2, .2, .6, .2))
    speedSet.append(LineSegment(.6, .2, .8, .4))
    speedSet.append(LineSegment(.8, .4, 100, .4))

#    x = [-10, 0, .2, .6, .8, 1000]
#    y = [0,   0, .2, .2, .4, .4]
#    new_x = np.linspace(0, 10, 10000)
#    new_y = sp.interpolate.interp1d(x, y, kind='cubic')(new_x)

#    speedSet.append(LineSegment(-100, 0, 0, 0))
#    for i in range(0, len(new_x) - 1):
#        if i == len(new_x) - 1 or new_x[i] > 2:
#            speedSet.append(LineSegment(new_x[i], new_y[i], 10000, new_y[i]))
#            break
#        speedSet.append(LineSegment(new_x[i], new_y[i], new_x[i+1], new_y[i+1]))

#    world = WorldState(0.2, 0.6, LineSegmentCalculator(speedSet))
    world = WorldState(0.28, 0.52, WolframAlphaCalculator())
    return world

def evaluate(output, genome, world, substrate):
    net = NEAT.NeuralNetwork()
    try:
        genome.BuildHyperNEATPhenotype(net, substrate)
        error = 0

        world.printStart(output)
        for i in range(NUMBER_OF_FRAMES):
            error += evaluateFrame(world, net)
            world.printStep(output)
            if world.mySpeed < 0:
                error *= 2
            #    output.write('#\n#\n#Final Fitness: 0\n')
            #    return 0
            world.stepInTime(TIME_PER_FRAME)

        if world.position <= 0:
            output.write('#\n#\n#Final Fitness: ')
            output.write(`world.position`)
            output.write('\n')
            return world.position

        error += world.getEndError()
        fitness = (MAX_FITNESS / (error + 1))
        world.printEnd(output)
        output.write('#\n#\n#Final Fitness: ')
        output.write(`fitness`)
        output.write('\n')
        return fitness
    except Exception as ex:
        print 'Error', ex
        output.write('#\n#Error: ')
        output.write(str(ex))
        output.write('\n#Final Fitness: 0\n')
        return 0

def getSubstrate():
    substrate = NEAT.Substrate([(-2, -1), (-1, -1), (-1, 0), (-1, 1), (-1, 2)],
                           [(0, -2), (0, -1), (0, 0), (0, 1), (0, 2)],
                           [(2, 0)])
    #substrate.m_allow_input_hidden_links = False
    #substrate.m_allow_input_output_links = False
    #substrate.m_allow_hidden_hidden_links = False
    #substrate.m_allow_hidden_output_links = False
    #substrate.m_allow_output_hidden_links = False
    #substrate.m_allow_output_output_links = False
    #substrate.m_allow_looped_hidden_links = False
    #substrate.m_allow_looped_output_links = False

    # let's configure it a bit to avoid recurrence in the substrate
    #substrate.m_allow_input_hidden_links = True
    #substrate.m_allow_input_output_links = True
    #substrate.m_allow_hidden_output_links = True
    #substrate.m_allow_hidden_hidden_links = True

    # let's set the activation functions
    #substrate.m_hidden_nodes_activation = NEAT.ActivationFunction.UNSIGNED_SIGMOID
    #substrate.m_outputs_nodes_activation = NEAT.ActivationFunction.UNSIGNED_SINE

    # when to output a link and max weight
    substrate.m_link_threshold = 0.2
    substrate.m_max_weight = 8.0

    return substrate

def createOutputFolder(count):
    if (count > 10):
        print 'Too many attempts to create output folder'
        return None
    millis = int(round(time.time() * 1000))
    path = 'output/' + `millis`
    if os.path.exists(path):
        print 'Path already exists, trying again'
        return createOutputFolder(count + 1)
    os.makedirs(path)
    return path

def main():
    path = createOutputFolder(1);
    if path is None:
        return

    # Set the random number seed
    rng = NEAT.RNG()
    rng.TimeSeed()

    print 'path:', path
    padding = len(str(NUMBER_OF_RUNS - 1))
    for i in range(NUMBER_OF_RUNS):
        run_path = path + '/' + `i`.zfill(padding)
        processRun(run_path, i)

def processRun(path, run):
    print 'Starting run', (run+1), '/', NUMBER_OF_RUNS
    os.makedirs(path)

    # Setup the params
    params = NEAT.Parameters()
    params.PopulationSize = NUMBER_OF_INDIVIDUALS

    # Create the substrate
    substrate = getSubstrate()

    g = NEAT.Genome(0,
                    substrate.GetMinCPPNInputs(),
                    0,
                    substrate.GetMinCPPNOutputs(),
                    False,
                    NEAT.ActivationFunction.SIGNED_GAUSS,
                    NEAT.ActivationFunction.SIGNED_GAUSS,
                    0,
                    params)

    pop = NEAT.Population(g, params, True, 1.0)
    padding = len(str(NUMBER_OF_GENS - 1))
    for generation in range(NUMBER_OF_GENS):
        generation_path = path + '/' + `generation`.zfill(padding)
        processGeneration(generation_path, generation, pop, substrate)

def processGeneration(path, generation, pop, substrate):
    os.makedirs(path)

    genome_list = NEAT.GetGenomeList(pop)
    padding = len(str(NUMBER_OF_INDIVIDUALS - 1))
    world = getNewWorld()
    for idx, genome in enumerate(genome_list):
        if SHOULD_SAVE_ALL:
            individual_path = path + '/' + `idx`.zfill(padding) + '.out'
            output = open(individual_path, 'w')
            fitness = evaluate(output, genome, world, substrate)
            output.close()
            genome.SetFitness(fitness)
        else:
            output = FakeWriter()
            fitness = evaluate(output, genome, world, substrate)
            genome.SetFitness(fitness)
        world.reset()

    best = max([x.GetLeader().GetFitness() for x in pop.Species])
    leader = filter(lambda x: x.GetLeader().GetFitness() == best, pop.Species)[0].GetLeader()

    best_path = path + '/best.out'
    output = open(best_path, 'w')
    fitness = evaluate(output, leader, world, substrate)
    output.close()

    gen_padding = len(str(NUMBER_OF_GENS - 1))
    print 'Generation:', `generation`.zfill(gen_padding), 'Best:', best, 'Calc:', fitness, 'Time:', time.time()

    pop.Epoch()

if __name__ == "__main__":
    main()
