#!/bin/bash

gcloud compute ssh --zone us-central1-b ai-2 --command "tar -c --bzip2 -f output.tar.bz2 work/ai/experiments/speed_up_at_yellow/output"
gcloud compute copy-files --zone us-central1-b ai-2:~/output.tar.bz2 .
tar xvf output.tar.bz2
expFile=`find ./work/ai/experiments/speed_up_at_yellow/output -name "best.out" | sort -n | tail -n 1`
./graphs.py "$expFile"
