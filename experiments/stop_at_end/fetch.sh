#!/bin/bash

gcloud compute ssh --zone us-central1-b ai-2 --command "tar -c --bzip2 -f output.tar.bz2 work/ai/experiments/stop_at_end/output"
gcloud compute copy-files --zone us-central1-b ai-2:~/output.tar.bz2 .
tar xvf output.tar.bz2
expFile=`find ./work/ai/experiments/stop_at_end/output -name "best.out" | sort -n | tail -n 1`
./graphs.py "$expFile"
