#!/usr/bin/python
import os
import sys
import time
import random as rnd
import commands as comm
import cv2
import numpy as np
import cPickle as pickle
import MultiNEAT as NEAT
import multiprocessing as mpc

NUMBER_OF_RUNS = 1
NUMBER_OF_GENS = 1000
NUMBER_OF_FRAMES = 500
NUMBER_OF_INDIVIDUALS = 300
TIME_PER_FRAME = 0.05
MAX_FITNESS = 1000

SHOULD_SAVE_ALL = False

class FakeWriter:
    def write(self, someStr):
        pass

    def close(self):
        pass

class WorldState:
    def __init__(self, middleSpeed, end, endSpeed):
        self.time = 0
        self.mySpeed = 0
        self.newSpeed = 0
        self.position = 0
        self.middleSpeed = middleSpeed
        self.endSpeed = endSpeed
        self.endLocation = end
        self.distanceToEnd = end
        self.distanceToStart = middleSpeed * 2.0
        self.desiredSpeed = 0
        self.desiredLocation = 0

    def stepInTime(self, delta):
        self.time += delta
        self.mySpeed = self.newSpeed
        self.position += delta * self.mySpeed
        fullSpeedLocation = self.middleSpeed * 2.0
        if (self.position <= fullSpeedLocation):
            self.desiredSpeed = self.position / 2.0
        elif (self.position >= self.endLocation):
            self.desiredSpeed = self.endSpeed
        elif (self.position >= (self.endLocation - fullSpeedLocation)):
            self.desiredSpeed = self.endSpeed + (self.middleSpeed) * (self.endLocation - self.position) / fullSpeedLocation
        else:
            self.desiredSpeed = self.middleSpeed

        # TODO(stoyan): Fix this calculation to not be based on adding
        self.desiredLocation += delta * self.desiredSpeed
        self.distanceToStart = fullSpeedLocation - self.position
        self.distanceToEnd = self.endLocation - self.position

    def printStart(self, output):
        output.write('#Time, Position, Speed, Error, Speed Diff, Desired Speed, Desired Location\n')
        self.printStep(output)

    def printStep(self, output):
        error = self.getError()
        speedDiff = self.newSpeed - self.mySpeed

        output.write(`self.time`)
        output.write(', ')
        output.write(`self.position`)
        output.write(', ')
        output.write(`self.mySpeed`)
        output.write(', ')
        output.write(`error`)
        output.write(', ')
        output.write(`speedDiff`)
        output.write(', ')
        output.write(`self.desiredSpeed`)
        output.write(', ')
        output.write(`self.desiredLocation`)
        output.write('\n')

    def getEndError(self):
        diff = self.endLocation - self.position
        return (abs(diff) + abs(min(0, diff))**2) * NUMBER_OF_FRAMES

    def getError(self):
        return abs(self.newSpeed - self.desiredSpeed) + abs(self.desiredLocation - self.position)

def evaluateFrame(state, net):
    # TODO(stoyan): Prety sure this is constant and always 2
    depth = 2

    net.Flush()
    net.Input([state.position, state.mySpeed, state.distanceToStart, state.distanceToEnd, 1])
    [net.Activate() for _ in range(depth)]
    o = net.Output()
    state.newSpeed = state.mySpeed + (o[0] - 0.5) * .5
    return state.getError()

def evaluate(output, genome, substrate):
    net = NEAT.NeuralNetwork()
    try:
        genome.BuildHyperNEATPhenotype(net, substrate)
        error = 0

        world = WorldState(0.05, 0.5, 0)
        world.printStart(output)
        for i in range(1, NUMBER_OF_FRAMES):
            error += evaluateFrame(world, net)
            world.printStep(output)
            if world.mySpeed < 0:
                output.write('#\n#\n#Final Fitness: 0\n')
                return 0
            world.stepInTime(TIME_PER_FRAME)

        if world.position <= 0:
            output.write('#\n#\n#Final Fitness: ')
            output.write(`world.position`)
            output.write('\n')
            return world.position

        error += world.getEndError()
        fitness = (MAX_FITNESS / (error + 1))
        output.write('#\n#\n#Final Fitness: ')
        output.write(`fitness`)
        output.write('\n')
        return fitness
    except Exception as ex:
        print 'Error', ex
        output.write('#\n#Error: ')
        output.write(str(ex))
        output.write('\n#Final Fitness: 0\n')
        return 0

def getSubstrate():
    substrate = NEAT.Substrate([(-2, -1), (-1, -1), (-1, 0), (-1, 1), (-1, 2)],
                           [(0, -2), (0, -1), (0, 0), (0, 1), (0, 2)],
                           [(2, 0)])
    #substrate.m_allow_input_hidden_links = False
    #substrate.m_allow_input_output_links = False
    #substrate.m_allow_hidden_hidden_links = False
    #substrate.m_allow_hidden_output_links = False
    #substrate.m_allow_output_hidden_links = False
    #substrate.m_allow_output_output_links = False
    #substrate.m_allow_looped_hidden_links = False
    #substrate.m_allow_looped_output_links = False

    # let's configure it a bit to avoid recurrence in the substrate
    #substrate.m_allow_input_hidden_links = True
    #substrate.m_allow_input_output_links = True
    #substrate.m_allow_hidden_output_links = True
    #substrate.m_allow_hidden_hidden_links = True

    # let's set the activation functions
    #substrate.m_hidden_nodes_activation = NEAT.ActivationFunction.UNSIGNED_SIGMOID
    #substrate.m_outputs_nodes_activation = NEAT.ActivationFunction.UNSIGNED_SINE

    # when to output a link and max weight
    substrate.m_link_threshold = 0.2
    substrate.m_max_weight = 8.0

    return substrate

def createOutputFolder(count):
    if (count > 10):
        print 'Too many attempts to create output folder'
        return None
    millis = int(round(time.time() * 1000))
    path = 'output/' + `millis`
    if os.path.exists(path):
        print 'Path already exists, trying again'
        return createOutputFolder(count + 1)
    os.makedirs(path)
    return path

def main():
    path = createOutputFolder(1);
    if path is None:
        return

    # Set the random number seed
    rng = NEAT.RNG()
    rng.TimeSeed()

    print 'path:', path
    padding = len(str(NUMBER_OF_RUNS - 1))
    for i in range(NUMBER_OF_RUNS):
        run_path = path + '/' + `i`.zfill(padding)
        processRun(run_path, i)

def processRun(path, run):
    print 'Starting run', (run+1), '/', NUMBER_OF_RUNS
    os.makedirs(path)

    # Setup the params
    params = NEAT.Parameters()
    params.PopulationSize = NUMBER_OF_INDIVIDUALS

    # Create the substrate
    substrate = getSubstrate()

    g = NEAT.Genome(0,
                    substrate.GetMinCPPNInputs(),
                    0,
                    substrate.GetMinCPPNOutputs(),
                    False,
                    NEAT.ActivationFunction.SIGNED_GAUSS,
                    NEAT.ActivationFunction.SIGNED_GAUSS,
                    0,
                    params)

    pop = NEAT.Population(g, params, True, 1.0)
    padding = len(str(NUMBER_OF_GENS - 1))
    for generation in range(NUMBER_OF_GENS):
        generation_path = path + '/' + `generation`.zfill(padding)
        processGeneration(generation_path, generation, pop, substrate)

def processGeneration(path, generation, pop, substrate):
    os.makedirs(path)

    genome_list = NEAT.GetGenomeList(pop)
    padding = len(str(NUMBER_OF_INDIVIDUALS - 1))
    for idx, genome in enumerate(genome_list):
        if SHOULD_SAVE_ALL:
            individual_path = path + '/' + `idx`.zfill(padding) + '.out'
            output = open(individual_path, 'w')
            fitness = evaluate(output, genome, substrate)
            output.close()
            genome.SetFitness(fitness)
        else:
            output = FakeWriter()
            fitness = evaluate(output, genome, substrate)
            genome.SetFitness(fitness)

    best = max([x.GetLeader().GetFitness() for x in pop.Species])
    leader = filter(lambda x: x.GetLeader().GetFitness() == best, pop.Species)[0].GetLeader()

    best_path = path + '/best.out'
    output = open(best_path, 'w')
    fitness = evaluate(output, leader, substrate)
    output.close()

    gen_padding = len(str(NUMBER_OF_GENS - 1))
    print 'Generation:', `generation`.zfill(gen_padding), 'Best:', best, 'Calc:', fitness, 'Time:', time.time()

    pop.Epoch()

if __name__ == "__main__":
    main()
