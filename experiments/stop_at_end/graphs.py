#!/usr/bin/python
import sys
from pylab import plotfile, show

def main():
    if len(sys.argv) != 2:
        print 'Must specify an experiment folder', len(sys.argv)
        return

    print 'path', sys.argv[1]

    fullPath = sys.argv[1]
    #fullPath = sys.argv[1] + '/0/00/best.out'
    plotme(fullPath)

def plotme(path):
    names = ['Time', 'Position', 'Speed', 'Error', 'Speed Diff', 'Expected Speed', 'Expected Position']
    plotfile(path, cols=('Time', 'Position', 'Expected Position', 'Error'), names=names, delimiter=',', subplots=False)
    plotfile(path, cols=('Time', 'Speed', 'Expected Speed'), names=names, delimiter=',', subplots=False)
    plotfile(path, cols=('Position', 'Speed', 'Expected Speed'), names=names, delimiter=',', subplots=False)
    show()

if __name__ == "__main__":
    main()
